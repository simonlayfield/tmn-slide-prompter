var projectData = {
    "active": 0,
    "class": "slideshow",
    "theme": "primary",
    "navigation": {
        "class": "navigation",
        "collection": [{
            "label": "Previous",
            "action": "switchActiveMinus",
            "class": "link"
        }, {
            "label": "Next",
            "action": "switchActivePlus",
            "class": "link"
        }]
    },
    "slides": {
        "collection": [{
            "label": "Atomic Design",
            "body": "<p>Atomic design is methodology for creating design systems.</p>",
            "class": "slide",
            "theme": "secondary",
            "collection": [{
                "label": "Atomic Design",
                "body": "<p>Atomic Design introduces a methodology for thinking of our UIs as thoughtful hierarchies, discusses the qualities of effective pattern libraries, and showcases techniques to transform your team's design and development workflow.</p><p><strong>In a lot of ways, this is how we've been doing things all along, even if we haven’t been consciously thinking about it in this specific way.</strong></p>",
                "active": false
            }]
        }, {
            "label": {
                "body": "The Old Way",
                "emphasis": "4",
                "class": "center",
                "theme": "secondary"
            },
            "class": "slide",
            "theme": "secondary",
            "body": {
                "sections": [{
                    "body": "<img src='./img/design-001.png' width='350px'>"
                }, {
                    "body": "<img src='./img/design-002.png' width='330px'>"
                }]

            }
        }, {
            "label": "The New Way",
            "collection": [{
                "body": "<img src='./img/atomic-design.gif'>",
                "active": false
            }, {
                "body": "<p>Atomic design gives us the ability to traverse from abstract to concrete. Because of this, we can create systems that promote consistency and scalability while simultaneously showing things in their final context. And by assembling rather than deconstructing, we’re crafting a system right out of the gate instead of cherry picking patterns after the fact.</p>"
            }],
            "class": "slide",
            "theme": "secondary"
        }, {
            "label": "Web Components",
            "class": "slide",
            "theme": "secondary",
            "collection": [{
                "label": null,
                "body": "<p>Web Components are a set of features currently being added by the W3C to the HTML and DOM specifications that allow for the creation of reusable widgets or components in web documents and web applications. The intention behind them is to bring component-based software engineering to the World Wide Web.</p>",
                "theme": "primary",
                "type": "strike"
            }, {
                "label": null,
                "body": "<p>Web Components are (ideally) small pieces of code that can be reused and given context, dependant on a set of meaningful data.</p>",
                "theme": "primary",
                "active": false
            }, {
                "label": "Lego 1",
                "body": "<img src='./img/lego-001.gif' height='200px'>",
                "active": false
            }, {
                "label": "Lego 2",
                "body": "<img src='./img/lego-002.gif' height='200px'>",
                "active": false
            }, {
                "label": "Lego 3",
                "body": "<img src='./img/lego-003.gif' height='200px'>",
                "active": false
            }]
        }, {
            "label": null,
            "class": "slide",
            "theme": "tertiary",
            "body": {
                "sections": [{
                    "body": "<img src='./img/code-001.png'>"
                }, {
                    "body": "<img src='./img/code-001.png'>"
                }]

            }
        }, {
            "label": null,
            "class": "slide",
            "theme": "tertiary",
            "body": {
                "sections": [{
                    "body": "<img src='./img/rcode-001.png'>"
                }, {
                    "body": "<img src='./img/rcode-002.png'>"
                }]
            }
        }, {
            "label": "For Designers",
            "body": {
                "label": {
                    "body": "Think use cases:",
                    "emphasis": "4",
                    "class": "sub",
                    "theme": "secondary"
                },
                "sections": [{
                    "body": "<img src='./img/icon-001.png' width='300px'>"
                }, {
                    "body": "<img src='./img/icon-002.png' width='300px'>"
                }, {
                    "body": "<img src='./img/icon-003.png' width='300px'>"
                }]
            },
            "class": "slide",
            "theme": "secondary"
        }]
    }
};
