window.onload = function () {
    progress();
};

var slideCounter = 0,
    colourCounter = 0,
    progressCounter = 0,
    slides = document.getElementsByClassName('slide'),
    colors = ['pink', 'blue', 'purple', 'orange', 'green'],
    progressLine = document.getElementById('progressLine'),
    progressWidth = 0;

function progress () {

    var progressVar = setInterval(progressTimer, 1000);

    document.body.classList.add(colors[0]);

    function progressTimer() {


        if (slideCounter === slides.length) {
            return;
        }

        progressCounter = progressCounter + 1;

        progressLine.style.width = (progressWidth + 1.66666667) + "%";
        progressWidth = progressWidth + 1.66666667;

        if (progressCounter == 60) {

            progressCounter = 0;
            progressLine.style.width = 0;
            progressWidth = 0;

            slideCounter = slideCounter + 1;

            if (colourCounter === colors.length - 1) {
                colourCounter = 0;

            } else {
                colourCounter = colourCounter + 1;
            }

            for (var i = 0; i < slides.length; i++) {
                slides[i].classList.remove('active');
             }

            slides[slideCounter].classList.add('active');

            document.body.classList.add(colors[colourCounter]);

        }

    }
}

function restartSlide () {
    progressCounter = 0;
    progressWidth = 0;
}

function nextSlide () {
    progressCounter = 59;
}
