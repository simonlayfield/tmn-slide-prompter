



var slideCounter = 0,
    colourCounter = 0,
    progressCounter = 0,
    slides = document.getElementsByClassName('slide'),
    colors = ['pink', 'blue', 'purple', 'orange', 'green'],
    progressLine = document.getElementById('progressLine'),
    progressWidth = 0,
    latch = false;

window.onload = function () {
    progress();
};

document.onkeydown = function checkKey (e) {

    if (e.keyCode == '32') {
        if (!latch) {
            latch = true;
            document.getElementById('pause').style.display = 'flex';
        } else {
            document.getElementById('pause').style.display = 'none';
            if (slideCounter%2) {
                changeSlide();
                latch = false;
            } else {
                latch = false;
            }

        }

    }
}

function changeSlide () {
    slideCounter = slideCounter + 1;

    if (colourCounter === colors.length - 1) {
        colourCounter = 0;

    } else {
        colourCounter = colourCounter + 1;
    }

    for (var i = 0; i < slides.length; i++) {
        slides[i].classList.remove('active');
     }

    slides[slideCounter].classList.add('active');

    document.body.classList.add(colors[colourCounter]);
}

function progress () {

    var progressVar = setInterval(progressTimer, 1000);

    document.body.classList.add(colors[0]);

    function progressTimer() {

        console.log(latch);

        if (latch) {
            return;
        }

        if (slideCounter === slides.length) {
            return;
        }

        progressCounter = progressCounter + 1;

        progressLine.style.width = (progressWidth + 0.8333333333333) + "%";
        progressWidth = progressWidth + 0.8333333333333;

        if (progressCounter == 120) {

            progressCounter = 0;
            progressLine.style.width = 0;
            progressWidth = 0;

            changeSlide();

            latch = true;

        }

    }
}

function restartSlide () {
    progressCounter = 0;
    progressWidth = 0;
}

function nextSlide () {
    progressCounter = 119;
}
