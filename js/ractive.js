
var heading = Ractive.extend({
    isolated: true,
    template: '#acHeading'
});

var anchor = Ractive.extend({
    isolated: true,
    template: '#acAnchor',
    onrender: function() {

        this.on('performAction', function(action) {

            if (action == 'switchActivePlus') {
                app.set('active', app.get('active') + 1);
            } else if (app.get('active') != 0 && action == 'switchActiveMinus') {
                app.set('active', app.get('active') - 1);
            }

        });

    }
});

var navigation = Ractive.extend({
    isolated: true,
    template: '#acNavigation',
    components: {
        "ac-anchor": anchor
    },
    onrender: function() {
        this.on('switchActivePlus', function() {
            console.log('This active Plus fired!');
        });
    }
});

var collection = Ractive.extend({
    isolated: true,
    template: '#acCollection'
});

var slideshow = Ractive.extend({
    isolated: true,
    template: '#acSlideshow',
    components: {
        "ac-heading": heading,
        "ac-collection": collection
    },
    onrender: function() {

        window.addEventListener("keypress", function(e) {
            var keyCode = e.keyCode;
            var activeSlide = app.get('active');
            var slide = app.get('slides.collection.' + activeSlide + '.collection');

            if (keyCode == 32 || keyCode == 49) {

                var lock = false;

                if (slide && keyCode == 32) {

                    var revealPoints = slide.map(function(obj, index) {

                        if (!lock && !obj.active) {
                            obj.active = true;
                            lock = true;
                        }
                        return obj;

                    });

                }

                if (!lock && keyCode == 32) {
                    app.set('active', app.get('active') + 1);
                }

                if (slide && keyCode == 49) {
                    var lock = false;
                    var revealPoints = slide.map(function(obj, index) {

                        if (!lock && obj.type == 'strike') {
                            obj.type = 'strikethrough';
                            lock = true;
                        }
                        return obj;

                    });
                }

                app.set('slides.collection.' + activeSlide + '.collection', revealPoints);

            }

        }, false);
    }
});

var app = new Ractive({
    el: 'app-container',
    template: '#template',
    data: projectData,
    components: {
        "ac-anchor": anchor,
        "ac-navigation": navigation,
        "ac-slideshow": slideshow
    }
});
